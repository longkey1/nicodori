package main

import (
	"io/ioutil"
	"strings"

	"gopkg.in/yaml.v2"
)

type Config struct {
	Command          string         `yaml:"execute-command"`
	IgnoreError      bool           `yaml:"ignore-error"`
	CacheDirectory   string         `yaml:"cache-directory"`
	ChannelIndexUrls []string       `yaml:"channel-index-urls"`
	ChannelFilters   ChannelFilters `yaml:"channel-filters"`
}

type ChannelFilters struct {
	Mode     string   `yaml:"mode"`
	Keywords []string `yaml:"keywords"`
}

func (fs *ChannelFilters) IsSkippedChannel(c Channel) bool {
	if fs.Mode == "exclude" {
		for _, k := range fs.Keywords {
			if strings.Contains(c.Title, k) {
				return true
			}
		}

		return false
	}

	if fs.Mode == "include" {
		for _, k := range fs.Keywords {
			if strings.Contains(c.Title, k) {
				return false
			}
		}

		return true
	}

	return false
}

func NewConfig(path string) (Config, error) {
	b, err := ioutil.ReadFile(path)
	if err != nil {
		return Config{}, err
	}

	c := Config{}
	err = yaml.Unmarshal(b, &c)
	if err != nil {
		return c, err
	}

	return c, nil
}
