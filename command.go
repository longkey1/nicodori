package main

import (
	"os/exec"
	"strings"

	shellwords "github.com/mattn/go-shellwords"
)

// Command ...
type Command struct {
	Content string
}

// Run ...
func (cmd *Command) Run() ([]byte, error) {
	c, err := shellwords.Parse(cmd.Content)
	if err != nil {
		return nil, err
	}

	switch len(c) {
	case 0:
		return nil, nil
	case 1:
		return exec.Command(c[0]).Output()
	default:
		return exec.Command(c[0], c[1:]...).Output()
	}
}

// NewCommand ...
func NewCommand(fmt string, c Channel, ri Rss2Item) Command {
	cmd := fmt
	cmd = strings.Replace(cmd, ":item_link", ri.Link, -1)
	cmd = strings.Replace(cmd, ":item_title", sanitizeTitle(ri.Title), -1)
	cmd = strings.Replace(cmd, ":channel_title", sanitizeTitle(c.Title), -1)
	cmd = strings.Replace(cmd, ":item_short_title", toShortTitle(ri.Title), -1)

	return Command{cmd}
}

func sanitizeTitle(str string) string {
	sanitized := str
	sanitized = strings.Replace(sanitized, "/", "／", -1)
	sanitized = strings.Replace(sanitized, "!", "！", -1)
	sanitized = strings.Replace(sanitized, ":", "：", -1)

	return sanitized
}

func toShortTitle(t string) string {
	title := sanitizeTitle(t)
	words := strings.Split(title, "　")
	if len(words) > 1 {
		return strings.Join(words[1:], "-")
	}

	return title
}
