# nicodori

[![wercker status](https://app.wercker.com/status/dd6c4d73715386f8490a8613336578b8/s/master "wercker status")](https://app.wercker.com/project/bykey/dd6c4d73715386f8490a8613336578b8)

![nicodori](https://cloud.githubusercontent.com/assets/58566/10476592/3bb9a8f6-728a-11e5-8eba-9d3726308c0b.png)

ニコニコ動画のアニメチャンネル配信番組一覧ページから動画情報を取得して、任意のコマンドを実行させるコマンドです。  
常駐しないので、CRONなどで定期実行することを想定しています。

## Description

- ニコニコ動画のアニメチャンネルのRSSしか動作確認してません
- 一度コマンドが実行されたアイテムは、次回以降実行されません
  - キャッシュを削除すれば、再度実行されるようになります
  - コマンドが異常終了した場合は、未実行扱いとなり次回も実行されます

## Usage

```
$ go get github.com/longkey1/nicodori
$ cp $GOPATH/src/github.com/longkey1/nicodori/config-sample.yml config.yml
$ nicodori
```

## Configulation

設定ファイル`config.yml`

- `execute-command`: 実行するコマンド。以下のキーワードをコマンド実行時に文字列へ変換する
    - `channel_title`: チャンネル名
    - `item_link`: アイテムのURL
    - `item_title`: アイテムのタイトル
    - `item_short_title`: `item_title`からチャンネル名の部分を取り除いたもの
- `ignore-error`: コマンドが異常終了した時に実行済み扱いして、次回からはコマンドが実行されないようにするかどうか
    - `true`: 異常終了しても無視して実行済みとする
    - `false`: 異常終了した場合、未実行扱いとする
- `cache-directory`: キャッシュファイルを保存するディレクトリ名
- `channel-index-urls`: 季節ごとの配信タイトル一覧ページのURL一覧
    - 例）2017年夏アニメ発表 - [http://ch.nicovideo.jp/ch_anime/blomaga/ar1278508](http://ch.nicovideo.jp/ch_anime/blomaga/ar1278508) など
- `channel-filters`:
    - `mode`: "include" or "exclude" どちらかのみ指定できます。
    - `keywords`: アイテムのタイトルに指定した文字列が含まれていた場合、`mode`によってスキップ対象とします。
