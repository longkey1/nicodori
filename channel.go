package main

import (
	"net/url"
	"regexp"
	"strings"

	"github.com/PuerkitoBio/goquery"
)

type Channel struct {
	Title string
	Url   *url.URL
}

func (c *Channel) Id() string {
	ss := strings.Split(c.Url.Path, "/")

	return ss[len(ss)-1]
}

type Channels []Channel

func (cs Channels) Has(ch Channel) bool {
	for _, c := range cs {
		if ch.Id() == c.Id() {
			return true
		}
	}

	return false
}

func NewChannel(url string) (Channel, error) {
	doc, err := goquery.NewDocument(url)
	if err != nil {
		return Channel{}, err
	}
	title := doc.Find("title").First().Text()
	title = strings.Replace(title, "- ニコニコチャンネル:アニメ", "", -1)
	title = regexp.MustCompile(`\[.+\]`).ReplaceAllString(title, "")
	title = strings.TrimSpace(title)

	return Channel{Title: title, Url: doc.Url}, nil
}

func NewChannels(urls []string) (Channels, error) {
	// to unique
	us := make(map[string]string)
	for _, u := range urls {
		us[u] = u
	}

	cs := Channels{}
	for _, u := range us {
		c, err := NewChannel(u)
		if err != nil {
			return Channels{}, err
		}
		cs = append(cs, c)
	}

	return cs, nil
}

func ParseChannelIndexPage(url string) ([]string, error) {
	var res []string

	doc, err := goquery.NewDocument(url)
	if err != nil {
		return res, err
	}

	reg := regexp.MustCompile(`^https?://ch.nicovideo.jp/ch[0-9]+`)
	doc.Find("#main_right a").Each(func(_ int, s *goquery.Selection) {
		u, _ := s.Attr("href")
		if true != reg.MatchString(u) {
			return
		}
		res = append(res, u)
	})

	return res, nil
}
