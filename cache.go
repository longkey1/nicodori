package main

import (
	"fmt"
	"io/ioutil"
	"os"

	"gopkg.in/yaml.v2"
)

type Cache struct {
	Path    string
	Content CacheContent
}

type CacheContent struct {
	Channel CacheChannel `yaml:"channel"`
	Items   []CacheItem  `yaml:"items"`
}

type CacheChannel struct {
	Title string `yaml:"title"`
	URL   string `yaml:"url"`
}

type CacheItem struct {
	Title string `yaml:"title"`
	Link  string `yaml:"link"`
}

func (c *Cache) Has(i Rss2Item) bool {
	for _, ci := range c.Content.Items {
		if i.Link == ci.Link {
			return true
		}
	}

	return false
}

func (c *Cache) Append(item Rss2Item) {
	i := CacheItem{Title: item.Title, Link: item.Link}
	c.Content.Items = append(c.Content.Items, i)
}

func (c *Cache) Save() error {
	yaml, err := yaml.Marshal(c.Content)
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(c.Path, []byte(yaml), os.ModePerm)
	if err != nil {
		return err
	}

	return nil
}

func InitCacheDirectory(path string) error {
	return os.MkdirAll(path, os.ModePerm)
}

func NewCache(cnf Config, ch Channel) (Cache, error) {
	c := Cache{Path: fmt.Sprintf("%s/%s", cnf.CacheDirectory, ch.Id())}

	_, err := os.Stat(c.Path)
	if err != nil {
		c.Content.Channel = CacheChannel{Title: ch.Title, URL: ch.Url.String()}

		return c, nil
	}

	buf, err := ioutil.ReadFile(c.Path)
	if err != nil {
		return c, err
	}

	err = yaml.Unmarshal(buf, &c.Content)
	if err != nil {
		return c, err
	}

	return c, nil
}
