package main

import (
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"net/http"
)

type Rss2 struct {
	Title       string     `xml:"channel>title"`
	Link        string     `xml:"channel>link"`
	Description string     `xml:"channel>description"`
	Items       []Rss2Item `xml:"channel>item"`
}

type Rss2Item struct {
	Title       string `xml:"title"`
	Link        string `xml:"link"`
	Description string `xml:"description"`
	Content     string `xml:"encoded"`
	PubDate     string `xml:"pubDate"`
}

func NewRss2(url string) (Rss2, error) {
	rss2 := Rss2{}

	res, err := http.Get(url)
	if err != nil {
		return rss2, err
	}

	buf, _ := ioutil.ReadAll(res.Body)
	err = xml.Unmarshal(buf, &rss2)
	if err != nil {
		return rss2, err
	}

	return rss2, nil
}

func BuildChannelVideoRssUrl(c Channel) string {
	return fmt.Sprintf("%s/video?rss=2.0", c.Url.String())
}
