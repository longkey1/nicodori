package main

import (
	"flag"
	"fmt"
	"log"
	"os"
)

const (
	version = "1.1.0"
)

func main() {
	// flags
	var configPath string
	flag.StringVar(&configPath, "config", "config.yml", "configuration file path")
	flag.StringVar(&configPath, "c", "config.yml", "configuration file path")
	var displayVersion bool
	flag.BoolVar(&displayVersion, "version", false, "display version")
	flag.BoolVar(&displayVersion, "v", false, "display version")
	flag.Parse()

	if displayVersion {
		fmt.Printf("nicodori version %s\n", version)
		os.Exit(0)
	}

	log.Println("nicodori start")

	// create config
	cnf, err := NewConfig(configPath)
	if err != nil {
		log.Fatalf("configuration file (%s) read error > %s\n", configPath, err.Error())
	}

	// create cache directory
	err = InitCacheDirectory(cnf.CacheDirectory)
	if err != nil {
		log.Fatalf("cache directory (%s) create error > %s\n", cnf.CacheDirectory, err.Error())
	}

	// create niconico anime channles
	chUrls := []string{}
	for _, iUrl := range cnf.ChannelIndexUrls {
		urls, err := ParseChannelIndexPage(iUrl)
		if err != nil {
			log.Fatalf("channel index page parse error\n > %s", err.Error())
		}
		chUrls = append(chUrls, urls...)
	}
	allChs, err := NewChannels(chUrls)
	if err != nil {
		log.Fatalf("create all channels error\n > %s", err.Error())
	}
	chs := []Channel{}
	for _, ch := range allChs {
		if cnf.ChannelFilters.IsSkippedChannel(ch) {
			continue
		}
		chs = append(chs, ch)
	}

	for _, ch := range chs {
		execute(cnf, ch)
	}

	log.Println("nicodori end")
}

func execute(cnf Config, ch Channel) {
	rss, err := NewRss2(BuildChannelVideoRssUrl(ch))
	if err != nil {
		log.Printf("channel (%s) feed get error > %s\n", ch.Url.String(), err.Error())
		return
	}

	if len(rss.Items) == 0 {
		log.Printf("%s item list is empty (%s) \n", ch.Title, ch.Url)
		return
	}

	cch, err := NewCache(cnf, ch)
	if err != nil {
		log.Printf("channel (%s) cache get error > %s\n", ch.Title, err.Error())
		return
	}

	for _, item := range rss.Items {
		if cch.Has(item) {
			continue
		}

		log.Printf("%s(%s) command start\n", item.Title, item.Link)
		cmd := NewCommand(cnf.Command, ch, item)
		_, err = cmd.Run()
		if err != nil {
			log.Printf("%s(%s) command error > %s\n", item.Title, item.Link, err.Error())
		} else {
			log.Printf("%s(%s) command end\n", item.Title, item.Link)
		}

		if err != nil && false == cnf.IgnoreError {
			continue
		}

		cch.Append(item)
		cch.Save()
	}
}
