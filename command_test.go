package main

import (
	"testing"
)

func TestNewCommand(t *testing.T) {
	cmdFmt := "echo \":item_title :item_link :channel_title :item_short_title\""
	c := Channel{
		Title: "チャンネル名",
	}
	ri := Rss2Item{
		Title: "タイトル名　サブタイトル",
		Link:  "http://example.com/link.html",
	}
	cmd := NewCommand(cmdFmt, c, ri)
	expected := Command{Content: "echo \"タイトル名　サブタイトル http://example.com/link.html チャンネル名 サブタイトル\""}
	if expected != cmd {
		t.Errorf("NewCommand error, Expected:%v, Actual:%v", expected, cmd)
	}
}

func TestCommandRun(t *testing.T) {
	cmd := Command{Content: "echo \"タイトル名　サブタイトル httl://example.com/link.html チャンネル名 サブタイトル\""}
	_, err := cmd.Run()
	if err != nil {
		t.Errorf("CommandRun error, %s", err.Error())
	}
}

func TestCommandRunError(t *testing.T) {
	cmd := Command{Content: "hogehoge \"タイトル名\""}
	_, err := cmd.Run()
	if err == nil {
		t.Error("CommandRun error, Should raise error")
	}
}
