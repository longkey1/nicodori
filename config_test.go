package main

import (
	"io/ioutil"
	"os"
	"reflect"
	"testing"
)

func TestNewConfg(t *testing.T) {
	path := "./config-sample.yml"
	cnf, err := NewConfig(path)
	if err != nil {
		t.Errorf("NewConfig file read error, %s", err.Error())
	}

	expected := Config{
		Command:        "path/to/command -link :item_link -file \"./:item_title.%(ext)s\"",
		IgnoreError:    false,
		CacheDirectory: "./.cache.d",
		ChannelIndexUrls: []string{
			"http://ch.nicovideo.jp/ch_anime/blomaga/ar1278508",
			"http://ch.nicovideo.jp/ch_anime/blomaga/ar1232760",
		},
		ChannelFilters: ChannelFilters{
			Mode: "include",
			Keywords: []string{
				"ハイキュー",
				"ヒーローアカデミア",
			},
		},
	}
	if reflect.DeepEqual(cnf, expected) == false {
		t.Errorf("NewConfig error, Expected: %v, Actual: %v", expected, cnf)
	}
}

func TestNewConfigNotExistError(t *testing.T) {
	path := "path/to/config.yml"
	_, err := NewConfig(path)
	if err == nil {
		t.Error("NewConfig error, Should raise error")
	}
}

func TestNewConfigFormatError(t *testing.T) {
	f := createConfigFile("foo\n")
	_, err := NewConfig(f.Name())
	if err == nil {
		t.Error("NewConfig error, Should raise error")
	}
}

func TestChannelFiltersIsSkippedChannel(t *testing.T) {
	cf := ChannelFilters{
		Mode: "include",
		Keywords: []string{
			"ハイキュー",
			"ヒーローアカデミア",
		},
	}

	var c Channel
	c = Channel{Title: "サンプルラジオ"}
	if false == cf.IsSkippedChannel(c) {
		t.Error("ChannelFiltersIsSkippedChannel error, Should be true")
	}

	c = Channel{Title: "ハイキュー セカンドシーズン"}
	if true == cf.IsSkippedChannel(c) {
		t.Error("ChannelFiltersIsSkippedChannel error, Should be false")
	}
}

func createConfigFile(c string) *os.File {
	f, _ := ioutil.TempFile(os.TempDir(), "nicodori-test-config-")
	b := []byte(c)
	_, _ = f.Write(b)

	return f
}
