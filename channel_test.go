package main

import (
	"net/url"
	"testing"
)

func TestChannelId(t *testing.T) {
	u, _ := url.Parse("http://ch.nicovideo.jp/hoge")
	ch := Channel{Url: u}

	expects := "hoge"
	if ch.Id() != expects {
		t.Error("Channel error, IdShould raise error")
	}
}

func TestChannelsHas(t *testing.T) {
	var u *url.URL
	var c Channel
	var cl Channels
	u, _ = url.Parse("http://ch.nicovideo.jp/ch1")
	c = Channel{Url: u}
	cl = append(cl, c)
	u, _ = url.Parse("http://ch.nicovideo.jp/ch2")
	c = Channel{Url: u}
	cl = append(cl, c)
	u, _ = url.Parse("http://ch.nicovideo.jp/ch3")
	c = Channel{Url: u}
	cl = append(cl, c)

	var target Channel
	var expects bool
	u, _ = url.Parse("http://ch.nicovideo.jp/ch2")
	target = Channel{Url: u}
	expects = true
	if cl.Has(target) != expects {
		t.Error("ChannelsHas error, Should raise error")
	}

	u, _ = url.Parse("http://ch.nicovideo.jp/ch4")
	target = Channel{Url: u}
	expects = false
	if cl.Has(target) != expects {
		t.Error("ChannelsHas error, Should raise error")
	}

}

func TestNewChannel(t *testing.T) {
	u := "http://ch.nicovideo.jp/guruguru-anime"
	c, _ := NewChannel(u)
	expects := "魔法陣グルグル"
	if c.Title != expects {
		t.Error("NewChanne error, Should raise error")
	}
}

func TestNewChannels(t *testing.T) {
	var us []string
	var expects string
	us = append(us, "http://ch.nicovideo.jp/guruguru-anime")
	us = append(us, "http://ch.nicovideo.jp/netsuzoutrap")

	cs, _ := NewChannels(us)
	expects = "魔法陣グルグル"
	if cs[0].Title != expects {
		t.Errorf("NewChannel error, Expected: %v, Actual: %v", expects, cs[0].Title)
	}
	expects = "捏造トラップ-NTR-"
	if cs[1].Title != expects {
		t.Errorf("NewChannel error, Expected: %v, Actual: %v", expects, cs[1].Title)
	}
}

func TestParseChannelIndexPage(t *testing.T) {
	u := "http://ch.nicovideo.jp/portal/anime"
	us, _ := ParseChannelIndexPage(u)
	if false == stringContains(us, "http://ch.nicovideo.jp/ch2630593") {
		t.Error("ParseChannelIndexPage error")
	}
	if false == stringContains(us, "http://ch.nicovideo.jp/ch2636433") {
		t.Error("ParseChannelIndexPage error")
	}
}

func stringContains(list []string, item string) bool {
	for _, i := range list {
		if item == i {
			return true
		}
	}

	return false
}
