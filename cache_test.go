package main

import (
	"fmt"
	"io/ioutil"
	"net/url"
	"os"
	"reflect"
	"strings"
	"testing"
)

func TestNewCache(t *testing.T) {
	// create cache file
	str := `channel:
  title: サンプルチャンネル
  url: %channel_url%
items:
- title: サンプルタイトル1
  link: http://www.nicovideo.jp/watch/1111111111
- title: サンプルタイトル2
  link: http://www.nicovideo.jp/watch/2222222222
`
	f := createCacheFile("")
	chID := strings.TrimLeft(f.Name(), fmt.Sprintf("%s/", os.TempDir()))
	chUrl, _ := url.Parse(fmt.Sprintf("http://ch.nicovideo.jp/%s", chID))
	str = strings.Replace(str, "%channel_url%", chUrl.String(), -1)
	f.Write([]byte(str))

	cnf := Config{CacheDirectory: os.TempDir()}
	ch := Channel{Title: "サンプルチャンネル", Url: chUrl}
	cch, err := NewCache(cnf, ch)
	if err != nil {
		t.Errorf("NewCache error, %s", err.Error())
	}

	expected := Cache{
		Path: fmt.Sprintf("%s/%s", cnf.CacheDirectory, chID),
		Content: CacheContent{
			Channel: CacheChannel{
				Title: ch.Title,
				URL:   ch.Url.String(),
			},
			Items: []CacheItem{
				CacheItem{Title: "サンプルタイトル1", Link: "http://www.nicovideo.jp/watch/1111111111"},
				CacheItem{Title: "サンプルタイトル2", Link: "http://www.nicovideo.jp/watch/2222222222"},
			},
		},
	}
	if reflect.DeepEqual(cch, expected) == false {
		t.Errorf("NewCache error, Expected: %v, Actual: %v", expected, cch)
	}
}

func TestNewCacheErrorNotYaml(t *testing.T) {
	str := `aaaaa,bbbbbb,ccccccc
dddd,eee,ffff
ggg,hh,iiiiii
`
	f := createCacheFile(str)
	cnf := Config{CacheDirectory: os.TempDir()}
	chID := strings.TrimLeft(f.Name(), fmt.Sprintf("%s/", os.TempDir()))
	url, _ := url.Parse(fmt.Sprintf("http://ch.nicovideo.jp/%s", chID))
	ch := Channel{Url: url}

	_, err := NewCache(cnf, ch)
	if err == nil {
		t.Error("NewCache error, Should raise error")
	}
}

func TestNewCacheNotExists(t *testing.T) {
	chID := "hoge"
	cnf := Config{CacheDirectory: os.TempDir()}
	url, _ := url.Parse(fmt.Sprintf("http://ch.nicovideo.jp/%s", chID))
	ch := Channel{Title: "チャンネルタイトル", Url: url}

	cch, err := NewCache(cnf, ch)
	if err != nil {
		t.Errorf("NewCache error, %s", err.Error())
	}

	expected := Cache{
		Path: fmt.Sprintf("%s/%s", cnf.CacheDirectory, chID),
		Content: CacheContent{
			Channel: CacheChannel{
				Title: ch.Title,
				URL:   ch.Url.String(),
			},
		},
	}
	if reflect.DeepEqual(cch, expected) == false {
		t.Errorf("NewCache error, Expected: %v, Actual: %v", expected, cch)
	}
}

func TestCacheSave(t *testing.T) {
	f := createCacheFile("")
	chID := strings.TrimLeft(f.Name(), fmt.Sprintf("%s/", os.TempDir()))
	cnf := Config{CacheDirectory: os.TempDir()}
	cc := Cache{
		Path: fmt.Sprintf("%s/%s", cnf.CacheDirectory, chID),
		Content: CacheContent{
			Channel: CacheChannel{
				Title: "サンプルチャンネル",
				URL:   fmt.Sprintf("http://ch.nicovideo.jp/%s", chID),
			},
			Items: []CacheItem{
				CacheItem{Title: "サンプルタイトル1", Link: "http://www.nicovideo.jp/watch/1111111111"},
				CacheItem{Title: "サンプルタイトル2", Link: "http://www.nicovideo.jp/watch/2222222222"},
				CacheItem{Title: "サンプルタイトル3", Link: "http://www.nicovideo.jp/watch/3333333333"},
			},
		},
	}

	err := cc.Save()
	if err != nil {
		t.Errorf("CacheSave error, %s", err.Error())
	}
	buf, _ := ioutil.ReadFile(f.Name())
	actual := string(buf)
	expected := `channel:
  title: サンプルチャンネル
  url: %channel_url%
items:
- title: サンプルタイトル1
  link: http://www.nicovideo.jp/watch/1111111111
- title: サンプルタイトル2
  link: http://www.nicovideo.jp/watch/2222222222
- title: サンプルタイトル3
  link: http://www.nicovideo.jp/watch/3333333333
`
	expected = strings.Replace(expected, "%channel_url%", fmt.Sprintf("http://ch.nicovideo.jp/%s", chID), -1)
	if reflect.DeepEqual(actual, expected) == false {
		t.Errorf("CacheSave error, Expected: %v, Actual: %v", expected, actual)
	}
}

func TestSaveCacheErrorNotWritable(t *testing.T) {
	id := "hoge"

	cnf := Config{CacheDirectory: "/path/to"}
	cc := Cache{
		Path: fmt.Sprintf("%s/%s", cnf.CacheDirectory, id),
		Content: CacheContent{
			Channel: CacheChannel{
				Title: "サンプルチャンネル",
				URL:   "http://ch.nicovideo.jp/sample",
			},
			Items: []CacheItem{
				CacheItem{Title: "サンプルタイトル1", Link: "http://www.nicovideo.jp/watch/1111111111"},
				CacheItem{Title: "サンプルタイトル2", Link: "http://www.nicovideo.jp/watch/2222222222"},
				CacheItem{Title: "サンプルタイトル3", Link: "http://www.nicovideo.jp/watch/3333333333"},
			},
		},
	}
	err := cc.Save()
	if err == nil {
		t.Error("CacheSave error, Should raise error")
	}
}

func TestCacheHas(t *testing.T) {
	cch := Cache{
		Content: CacheContent{
			Items: []CacheItem{
				CacheItem{Title: "サンプルタイトル1", Link: "http://www.nicovideo.jp/watch/1111111111"},
				CacheItem{Title: "サンプルタイトル2", Link: "http://www.nicovideo.jp/watch/2222222222"},
			},
		},
	}
	cachedItem := Rss2Item{Link: "http://www.nicovideo.jp/watch/1111111111"}
	if cch.Has(cachedItem) == false {
		t.Error("CacheHas error, Should raise error")
	}
	noCachedItem := Rss2Item{Link: "http://www.nicovideo.jp/watch/1111111112"}
	if cch.Has(noCachedItem) == true {
		t.Error("CacheHas error, Should raise error")
	}
}

func TestAppend(t *testing.T) {
	item1 := Rss2Item{Title: "サンプルタイトル1", Link: "http://www.nicovideo.jp/watch/1111111111"}
	item2 := Rss2Item{Title: "サンプルタイトル2", Link: "http://www.nicovideo.jp/watch/2222222222"}
	cc := Cache{}
	cc.Append(item1)
	cc.Append(item2)
	expected := Cache{
		Content: CacheContent{
			Items: []CacheItem{
				CacheItem{Title: "サンプルタイトル1", Link: "http://www.nicovideo.jp/watch/1111111111"},
				CacheItem{Title: "サンプルタイトル2", Link: "http://www.nicovideo.jp/watch/2222222222"},
			},
		},
	}
	if reflect.DeepEqual(cc, expected) == false {
		t.Errorf("Append error, Expected %v, Actual %v", expected, cc)
	}
}

func createCacheFile(s string) *os.File {
	f, _ := ioutil.TempFile(os.TempDir(), "nicodori-cache-")
	buf := []byte(s)
	_, _ = f.Write(buf)

	return f
}
