package main

import (
	"net/url"
	"strings"
	"testing"
)

func TestNewRss2(t *testing.T) {
	u := "http://static.userland.com/gems/backend/rssTwoExample2.xml"
	rss, err := NewRss2(u)
	if err != nil {
		t.Errorf("NewRss2 error, %s", err.Error())
	}

	expected := "Scripting News"
	if rss.Title != expected {
		t.Errorf("NewRss2 error, Expected: %v, Actual: %v", expected, rss.Title)
	}

	expected = "http://www.scripting.com/"
	if rss.Link != expected {
		t.Errorf("NewRss2 error, Expected: %v, Actual: %v", expected, rss.Link)
	}

	expected = "A weblog about scripting and stuff like that."
	if rss.Description != expected {
		t.Errorf("NewRss2 error, Expected: %v, Actual: %v", expected, rss.Description)
	}

	if len(rss.Items) != 9 {
		t.Errorf("NewRss2 error, Expected: not %v, Actual: %v", 9, len(rss.Items))
	}
	// item
	item := rss.Items[6]
	expected = "Law and Order"
	if item.Title != expected {
		t.Errorf("NewRss2 error, Expected not: %v, Actual: %v", expected, item.Title)
	}

	expected = "http://scriptingnews.userland.com/backissues/2002/09/29#lawAndOrder"
	if item.Link != expected {
		t.Errorf("NewRss2 error, Expected: %v, Actual: %v", expected, item.Link)
	}

	expected = "<p><a href=\"http://www.nbc.com/Law_&_Order/index.html\"><img src=\"http://radio.weblogs.com/0001015/images/2002/09/29/lenny.gif\" width=\"45\" height=\"53\" border=\"0\" align=\"right\" hspace=\"15\" vspace=\"5\" alt=\"A picture named lenny.gif\"></a>A great line in a recent Law and Order. Lenny Briscoe, played by Jerry Orbach, is interrogating a suspect. The suspect tells a story and reaches a point where no one believes him, not even the suspect himself. Lenny says: \"Now there's five minutes of my life that's lost forever.\" </p>"
	if strings.Contains(item.Description, expected) == false {
		t.Errorf("NewRss2 error, Expected contains: %v, Actual: %v", expected, item.Description)
	}

	expected = "Sun, 29 Sep 2002 23:48:33 GMT"
	if item.PubDate != expected {
		t.Errorf("NewRss2 error, Expected: %v, Actual: %v", expected, item.PubDate)
	}
}
func TestNewRss2NotFoundError(t *testing.T) {
	u := "example.com/rss2.xml"
	_, err := NewRss2(u)
	if err == nil {
		t.Error("NewRss2 error, Should raise error")
	}
}
func TestNewRss2FormatError(t *testing.T) {
	_, err := NewRss2("https://httpbin.org/anything")
	if err == nil {
		t.Error("NewRss2 error, Should raise error")
	}
}

func TestBuildChannelVideoRssUrl(t *testing.T) {
	u, _ := url.Parse("http://example.com")
	c := Channel{Url: u}

	expected := "http://example.com/video?rss=2.0"
	ru := BuildChannelVideoRssUrl(c)
	if ru != expected {
		t.Errorf("BuildChannelVideoRssUrl error, Expected: %v, Actual: %v", expected, ru)
	}
}
